#include "pch.h"
#include <iostream>

#include <SKGraphics/Image/lodepng.h>
#include <SKGraphics/Renderer.h>
#include <SKSystem/IO/FileStream.h>
#include <SKSystem/IO/FileSystem.h>
#include <SKSystem/IO/MemoryStream.h>
#include <SKSystem/Text/Encoding.h>
#include <SKSystem/Threading/Thread.h>
#include <SKSystem/Win32/Win32.h>
#include <algorithm>
#include <DirectXPackedVector.h>
#include <iostream>
#include <iomanip>

#define _USE_MATH_DEFINES
#include <math.h>

//#define HEIGHTMAP_DOUBLEPRECISION

//-----------------------------------------------------------------------------
namespace NormalToHeight {
	//-----------------------------------------------------------------------------

	//-----------------
	// Types
	//-----------------
	enum class PixelDir
	{
		North,
		NorthEast,
		East,
		SouthEast,
		South,
		SouthWest,
		West,
		NorthWest
	};

	enum class EChannel : uint8_t
	{
		Red,
		Green,
		Blue,
		Alpha,
		OneMinus_Red,
		OneMinus_Green,
		OneMinus_Blue,
		OneMinus_Alpha,
		None,
	};

	enum class EZRange : uint8_t
	{
		MinusOne_to_One,
		Zero_to_One,
		Clamp_Zero_to_One
	};

	enum class EEdgeMode : uint8_t
	{
		Free,
		Clamp,
		Wrap
	};

	class ChannelMapping
	{
	public:
		EChannel X;
		EChannel Y;
		EChannel Z;
		EZRange ZRange;

		bool operator==(const ChannelMapping& other) const
		{
			return X == other.X && Y == other.Y && Z == other.Z;
		}

		static const ChannelMapping& Default()
		{
			static ChannelMapping _DefaultMapping = { EChannel::Red, EChannel::Green, EChannel::Blue, EZRange::MinusOne_to_One };
			return _DefaultMapping;
		}

		struct MapIndices
		{
			int X;
			int Y;
			int Z;
		};

		static int GetIndexForChannel(EChannel channel)
		{
			switch (channel)
			{
			case NormalToHeight::EChannel::Red:
			case NormalToHeight::EChannel::OneMinus_Red:
				return 0;

			case NormalToHeight::EChannel::Green:
			case NormalToHeight::EChannel::OneMinus_Green:
				return 1;

			case NormalToHeight::EChannel::Blue:
			case NormalToHeight::EChannel::OneMinus_Blue:
				return 2;

			case NormalToHeight::EChannel::Alpha:
			case NormalToHeight::EChannel::OneMinus_Alpha:
				return 3;
			}

			return -1;
		}

		static bool FlipChannel(EChannel channel)
		{
			if (channel == EChannel::OneMinus_Alpha || channel == EChannel::OneMinus_Blue ||
				channel == EChannel::OneMinus_Green || channel == EChannel::OneMinus_Red)
			{
				return true;
			}

			return false;
		}

		MapIndices GetIndices(SK::Graphics::InternalFormat format) const
		{
			MapIndices map;
			if (format == SK::Graphics::InternalFormat::RGBA || format == SK::Graphics::InternalFormat::RGB)
			{
				//Straight copy
				map.X = GetIndexForChannel(X);
				map.Y = GetIndexForChannel(Y);
				map.Z = GetIndexForChannel(Z);
			}
			else
			{
				auto IsBlueOrRed = [](EChannel channel) {
					return channel == EChannel::Blue || channel == EChannel::OneMinus_Blue || channel == EChannel::Red || channel == EChannel::OneMinus_Red;
				};

				if (IsBlueOrRed(X))
					map.X = 2 - GetIndexForChannel(X);
				else
					map.X = GetIndexForChannel(X);

				if (IsBlueOrRed(Y))
					map.Y = 2 - GetIndexForChannel(Y);
				else
					map.Y = GetIndexForChannel(Y);

				if (IsBlueOrRed(Z))
					map.Z = 2 - GetIndexForChannel(Z);
				else
					map.Z = GetIndexForChannel(Z);
			}

			return map;
		}
	};

	//-----------------
	// Data
	//-----------------
	namespace
	{
		//Inputs
		bool  NormaliseHeight = false;
		float Scale = 1.0f;
		int   NumPasses = 2048;
		float MaxStepHeight = 50;
		float NormalScale = 1.0f;

		//Working data
		SK::Graphics::Renderer SKRenderer;
		SK::Graphics::VertexBufferPtr FullscreenQuad;
		size_t PShaderUpdateHeights = -1;
		size_t PShaderUpscaleHeight = -1;
		size_t PShaderGenerateDeltas = -1;
		size_t PShaderGenNormalMip = -1;

		long TotalNumPasses = 0;
		long NumPassesComplete = 0;
	}

	//-----------------
	// Functions
	//-----------------
	void SaveTexture(SK::IO::Stream& stream, SK::Graphics::TexturePtr texture);
	SK::Math::float3 GetNormal(const uint16_t* colour);
	float GetHeightFromAngle(float angleDot);
	float GetPixelDelta(const uint16_t* normalMap, int stride, SK::Math::int2 dim, int x, int y, PixelDir dir);
	void UpdateHeightMap(const SK::Math::int2& Dim, SK::Graphics::TexturePtr HeightMapTexture, SK::Graphics::TexturePtr NormalMapTexture);

	void Convert(SK::IO::Path& inFile, SK::IO::Path& outFile, SK::IO::Path& maskFile, int numPasses, bool normalise, float scale, float normalScale, float maxStepHeight, EEdgeMode edgeMode, ChannelMapping& mapping, bool outputRaw)
	{
		NumPasses = numPasses;
		NormaliseHeight = normalise;
		Scale = scale;
		NormalScale = normalScale;
		MaxStepHeight = maxStepHeight;

		//-- Renderer
		SK::GUI::Window NullWindow("SKNormalMap", SK::Math::int2(64, 64), SK::GUI::WindowStyle::NoBorder);

		SK::Graphics::RenderSettings Settings;
		Settings.Width = 64;
		Settings.Height = 64;
		Settings.PresentInterval = 0;
		Settings.Multisample = SK::Graphics::MultisampleType::None;
		Settings.Windowed = true;
		Settings.RationalRefreshRate = SK::Math::int2(0, 0);
		SKRenderer.Initialise(NullWindow, Settings);

		//-- Shaders
		SK::String Errors;
		SK::IO::FileStream ShaderFile(SK::IO::Path(SK::IO::SpecialFolder::Install, "Shaders.hlsl"), SK::IO::FileMode::Read);
		if (!ShaderFile.IsOpen())
		{
			std::cout << "Couldn't find Shaders.hlsl :(" << std::endl;
			return;
		}

		uint32_t ShaderLen = static_cast<uint32_t>(ShaderFile.Length());
		uint8_t* ShaderBytes = new uint8_t[ShaderLen + 1];
		ShaderFile.ReadData(ShaderBytes, 0, ShaderLen);
		ShaderFile.Close();

		SK::String ShaderDefines;
		if (edgeMode == EEdgeMode::Clamp)
		{
			ShaderDefines.Concat("CLAMP_EDGES;");
		}
		else if (edgeMode == EEdgeMode::Wrap)
		{
			ShaderDefines.Concat("WRAPPED_TEXTURE;");
		}

		//Vertex Shader
		auto VSCode = SKRenderer.CompileShader(ShaderBytes, ShaderLen, "Standard2D", ShaderDefines, SK::Graphics::FeatureLevel::vs_5_0, &Errors);
		if (!Errors.IsEmpty())
			SK::Debug::Print(Errors);
		auto VShader2D = SKRenderer.CreateVertexShader("2D", VSCode, static_cast<uint32_t>(VSCode.Length()), SK::Graphics::InputLayout::Vertex2D);

		//Update Heights
		auto PSCode = SKRenderer.CompileShader(ShaderBytes, ShaderLen, "UpdateHeights", ShaderDefines, SK::Graphics::FeatureLevel::ps_5_0, &Errors);
		if (!Errors.IsEmpty())
			SK::Debug::Print(Errors);
		PShaderUpdateHeights = SKRenderer.CreatePixelShader("UpdateHeights", PSCode, static_cast<uint32_t>(PSCode.Length()));

		//Upscale Heights
		PSCode = SKRenderer.CompileShader(ShaderBytes, ShaderLen, "UpscaleHeight", ShaderDefines, SK::Graphics::FeatureLevel::ps_5_0, &Errors);
		if (!Errors.IsEmpty())
			SK::Debug::Print(Errors);
		PShaderUpscaleHeight = SKRenderer.CreatePixelShader("UpscaleHeight", PSCode, static_cast<uint32_t>(PSCode.Length()));

		//Generate Deltas
		PSCode = SKRenderer.CompileShader(ShaderBytes, ShaderLen, "GenerateDeltas", ShaderDefines, SK::Graphics::FeatureLevel::ps_5_0, &Errors);
		if (!Errors.IsEmpty())
			SK::Debug::Print(Errors);
		PShaderGenerateDeltas = SKRenderer.CreatePixelShader("GenerateDeltas", PSCode, static_cast<uint32_t>(PSCode.Length()));

		//Generate Mip
		PSCode = SKRenderer.CompileShader(ShaderBytes, ShaderLen, "GenNormalMip", ShaderDefines, SK::Graphics::FeatureLevel::ps_5_0, &Errors);
		if (!Errors.IsEmpty())
			SK::Debug::Print(Errors);
		PShaderGenNormalMip = SKRenderer.CreatePixelShader("GenNormalMip", PSCode, static_cast<uint32_t>(PSCode.Length()));

		std::cout << "Built shaders" << std::endl;

		//Fullscreen Quad
		SK::Graphics::Vertex2D PP[4] =
		{
			{ SK::Math::float2(1, 1), SK::Math::float2(1, 0), SK::Graphics::ColourFloat(1, 1, 1, 1) },
			{ SK::Math::float2(1, -1), SK::Math::float2(1, 1), SK::Graphics::ColourFloat(1, 1, 1, 1) },
			{ SK::Math::float2(-1, 1), SK::Math::float2(0, 0), SK::Graphics::ColourFloat(1, 1, 1, 1) },
			{ SK::Math::float2(-1, -1), SK::Math::float2(0, 1), SK::Graphics::ColourFloat(1, 1, 1, 1) },
		};
		FullscreenQuad = SKRenderer.CreateVertexBuffer(SK::Graphics::VertexType::Vertex2D, 4, false, &PP, SK::Graphics::VertexSize(SK::Graphics::VertexType::Vertex2D) * 4);

		//Setup renderer state
		SKRenderer.SetVertexShader(VShader2D);
		SKRenderer.SetPixelShader(PShaderUpdateHeights);
		SKRenderer.SetInputLayout(SK::Graphics::InputLayout::Vertex2D);
		SKRenderer.SetShaderSampler(0, SK::Graphics::SamplerState::Point);

		SK::IO::FileStream NormalMapFile(inFile, SK::IO::FileMode::Read);
		SK::Graphics::Image NormalMap = SK::Graphics::Image::Load(NormalMapFile, NormalMapFile.Length());

		NormalMapFile.Close();
		int NMapStride = 4;
		if (NormalMap.Format() == SK::Graphics::InternalFormat::BGR || NormalMap.Format() == SK::Graphics::InternalFormat::RGB)
			NMapStride = 3;

		//Fill out normal map
		SK::Graphics::TexturePtr NormalMapTexture = SKRenderer.CreateTexture(NormalMap.Size(), SK::Graphics::TextureFormat::RGBA_64, SK::Graphics::TextureFlag::Lockable);

		auto TexResource = SKRenderer.LockTexture(NormalMapTexture, 0);
		uint8_t* TextureBuff = static_cast<uint8_t*>(TexResource.Data);
		{
			SK::IO::MemoryStream Reader(NormalMap.Data(), NormalMap.Size().x * NormalMap.Size().y * (NormalMap.BitDepth() / 8) * NMapStride);
			Reader.Endianness(SK::IO::Endian::Big);

			ChannelMapping::MapIndices Map = mapping.GetIndices(NormalMap.Format());
			float NormalSign = NormalScale > 0.0f ? 1.0f : -1.0f;
			for (int y = 0; y < NormalMap.Size().y; ++y)
			{
				uint16_t* TextureBuffRow16 = reinterpret_cast<uint16_t*>(TextureBuff + (y * TexResource.RowPitch));
				for (int x = 0; x < NormalMap.Size().x; ++x)
				{
					float Col[4];
					if (NormalMap.BitDepth() == 8)
					{
						Col[0] = Reader.Read<uint8_t>() / 255.0f;
						Col[1] = Reader.Read<uint8_t>() / 255.0f;
						Col[2] = Reader.Read<uint8_t>() / 255.0f;
						if (NMapStride == 4)
							Col[3] = Reader.Read<uint8_t>() / 255.0f;
					}
					else
					{
						Col[0] = Reader.Read<uint16_t>() / 65535.0f;
						Col[1] = Reader.Read<uint16_t>() / 65535.0f;
						Col[2] = Reader.Read<uint16_t>() / 65535.0f;
						if (NMapStride == 4)
							Col[3] = Reader.Read<uint16_t>() / 65535.0f;
					}


					float X = Col[Map.X];
					float Y = Col[Map.Y];
					if (ChannelMapping::FlipChannel(mapping.X))
						X = 1.0f - X;
					if (ChannelMapping::FlipChannel(mapping.Y))
						Y = 1.0f - Y;

					float X2 = ((2.0f * X) - 1.0f);
					float Y2 = ((2.0f * Y) - 1.0f);
					float Z2 = 0.0f;

					if (mapping.Z != EChannel::None)
					{
						float Z = Col[Map.Z];
						if (ChannelMapping::FlipChannel(mapping.Z))
							Z = 1.0f - Z;

						if (mapping.ZRange == EZRange::MinusOne_to_One)
							Z2 = ((2.0f * Z) - 1.0f);
						else if (mapping.ZRange == EZRange::Zero_to_One)
							Z2 = Z;
						else if (mapping.ZRange == EZRange::Clamp_Zero_to_One)
							Z2 = std::max(0.0f, ((2.0f * Z) - 1.0f));
					}
					else
					{
						Z2 = sqrt(std::max(0.0f, 1.0f - ((X2*X2) + (Y2*Y2))));
					}

					if (NormalScale != 1.0f)
					{
						float Len = sqrt((X2*X2) + (Y2*Y2));
						if (Len > 1.0f)
						{
							X2 /= Len;
							Y2 /= Len;
							Len = 1.0f;
						}

						float Angle = std::max(-float(M_PI_2), std::min(float(M_PI_2), acos(1.0f - Len) * NormalScale));
						float NewLen = (1.0f - cos(Angle));
						float ComponentScale = NewLen / Len;
						X2 *= ComponentScale;
						Y2 *= ComponentScale;

						float OldSign = Z2 > 0.0f ? 1.0f : -1.0f;
						Z2 = sqrt(std::max(0.0f, 1.0f - ((X2*X2) + (Y2*Y2)))) * OldSign * NormalSign;
					}

					const uint32_t Pos = x * 4;
					TextureBuffRow16[Pos] = DirectX::PackedVector::XMConvertFloatToHalf((X2 + 1.0f) / 2.0f);
					TextureBuffRow16[Pos+1] = DirectX::PackedVector::XMConvertFloatToHalf((Y2 + 1.0f) / 2.0f);
					TextureBuffRow16[Pos+2] = DirectX::PackedVector::XMConvertFloatToHalf((Z2 + 1.0f) / 2.0f);
					TextureBuffRow16[Pos+3] = DirectX::PackedVector::XMConvertFloatToHalf(1.0f);
				}
			}
		}
		SKRenderer.UnlockTexture(NormalMapTexture, 0);

		SK::IO::FileStream MaskFile(maskFile, SK::IO::FileMode::Read);
		SK::Graphics::TexturePtr MaskTexture = nullptr;
		if (MaskFile.IsOpen())
		{
			SK::Graphics::Image Mask = SK::Graphics::Image::Load(MaskFile, MaskFile.Length());
			MaskFile.Close();

			int MaskStride = 4;
			if (Mask.Format() == SK::Graphics::InternalFormat::BGR || Mask.Format() == SK::Graphics::InternalFormat::RGB)
				MaskStride = 3;

			if (Mask.Size() != SK::Math::int2(0, 0))
			{
				//Fill out mask
				MaskTexture = SKRenderer.CreateTexture(Mask.Size(), SK::Graphics::TextureFormat::Red_8, SK::Graphics::TextureFlag::Lockable);

				auto MaskResource = SKRenderer.LockTexture(MaskTexture, 0);
				uint8_t* MaskTextureBuff = static_cast<uint8_t*>(MaskResource.Data);
				if (Mask.BitDepth() == 8)
				{
					for (int y = 0; y < Mask.Size().y; ++y)
					{
						for (int x = 0; x < Mask.Size().x; ++x)
						{
							int TexPos = (MaskResource.RowPitch * y) + x;
							int ReaderPos = ((y * Mask.Size().x) + x) * MaskStride;
							MaskTextureBuff[TexPos] = Mask.Data()[ReaderPos] > 0 ? 255 : 0;
						}
					}
				}
				else
				{
					SK::IO::MemoryStream Reader(Mask.Data(), Mask.Size().x * Mask.Size().y * 2 * MaskStride);
					Reader.Endianness(SK::IO::Endian::Big);
					for (int y = 0; y < Mask.Size().y; ++y)
					{
						for (int x = 0; x < Mask.Size().x; ++x)
						{
							int TexPos = (MaskResource.RowPitch * y) + x;
							int ReaderPos = ((y * Mask.Size().x) + x) * MaskStride * 2;
							
							Reader.Position(ReaderPos);
							MaskTextureBuff[TexPos] = Reader.Read<uint16_t>() > 0 ? 255 : 0;
						}
					}
				}
				SKRenderer.UnlockTexture(MaskTexture, 0);
			}
		}

		if (MaskTexture == nullptr)
		{
			MaskTexture = SKRenderer.CreateTexture(NormalMap.Size(), SK::Graphics::TextureFormat::Red_8, SK::Graphics::TextureFlag::Lockable);
			auto MaskResource = SKRenderer.LockTexture(MaskTexture, 0);
			uint8_t* MaskTextureBuff = static_cast<uint8_t*>(MaskResource.Data);
			memset(MaskTextureBuff, 255, MaskResource.DepthPitch);
			SKRenderer.UnlockTexture(MaskTexture, 0);
		}

		SKRenderer.SetPixelShader(PShaderUpdateHeights);
		SKRenderer.SetShaderTexture(5, MaskTexture);
		SKRenderer.SetPixelShader(PShaderGenerateDeltas);
		SKRenderer.SetShaderTexture(5, MaskTexture);

		std::cout << "Loaded images" << std::endl;

		SK::Math::int2 Dim = NormalMap.Size();
		SK::Graphics::TexturePtr PrevHeightTex = nullptr;

		int NumMips = 0;
		{
			int MinEdge = std::min(Dim.x, Dim.y);
			while (MinEdge > 256)
			{
				MinEdge /= 2;
				++NumMips;
			}
		}

		TotalNumPasses = (NumMips + 1) * NumPasses;
		NumPassesComplete = 0;

		for (int MipLevel = NumMips; MipLevel >= 0; --MipLevel)
		{
			int StepSize = static_cast<int>(pow(2, MipLevel));
			SK::Math::int2 MipDim(Dim.x / StepSize, Dim.y / StepSize);

			auto MipNormalTexture = SKRenderer.CreateTexture(MipDim, SK::Graphics::TextureFormat::RGBA_64, { SK::Graphics::TextureFlag::RenderTarget });
			SKRenderer.SetViewport(SK::Math::int2(0, 0), MipDim);
			SKRenderer.CommitShaderParams();

			//Make scaled down normal map
			int Stride = NMapStride;
			if (MipLevel > 0)
			{
				SKRenderer.SetPixelShader(PShaderGenNormalMip);
				SKRenderer.SetShaderTexture(4, NormalMapTexture);
				SKRenderer.SetInputLayout(SK::Graphics::InputLayout::Vertex2D);
				SKRenderer.SetShaderSampler(0, SK::Graphics::SamplerState::TriLinearClamped);
				SKRenderer.CommitShaderParams();
				SKRenderer.SetRenderTarget(MipNormalTexture, 0, nullptr);
				SKRenderer.DrawBuffer(SK::Graphics::PrimType::TriangleStrip, FullscreenQuad, 4, 0);
				SKRenderer.SetPixelShader(PShaderUpdateHeights);
				SKRenderer.SetShaderTexture(4, MipNormalTexture);
			}
			else
			{
				MipNormalTexture = NormalMapTexture;
			}

#ifdef HEIGHTMAP_DOUBLEPRECISION
			auto MipHeightTexture = SKRenderer.CreateTexture(MipDim, SK::Graphics::TextureFormat::RG_UINT64, { SK::Graphics::TextureFlag::RenderTarget });
#else
			auto MipHeightTexture = SKRenderer.CreateTexture(MipDim, SK::Graphics::TextureFormat::Red_32, { SK::Graphics::TextureFlag::RenderTarget });
#endif

			//Scale up previous mip
			if (PrevHeightTex != nullptr)
			{
				SKRenderer.SetPixelShader(PShaderUpscaleHeight);
				SKRenderer.SetShaderTexture(0, PrevHeightTex);
				SKRenderer.SetInputLayout(SK::Graphics::InputLayout::Vertex2D);
				SKRenderer.SetShaderSampler(0, SK::Graphics::SamplerState::TriLinearClamped);
				SKRenderer.CommitShaderParams();
				SKRenderer.SetRenderTarget(MipHeightTexture, 0, nullptr);
				SKRenderer.DrawBuffer(SK::Graphics::PrimType::TriangleStrip, FullscreenQuad, 4, 0);
				SKRenderer.SetPixelShader(PShaderUpdateHeights);
			}
			else
			{
				//Nope, gave up on initialising the texture, just start from 0
#ifdef HEIGHTMAP_DOUBLEPRECISION
				auto HeightInitTexture = SKRenderer.CreateTexture(MipDim, SK::Graphics::TextureFormat::RG_UINT64, { SK::Graphics::TextureFlag::Lockable });
				int Stride = 8;
#else
				auto HeightInitTexture = SKRenderer.CreateTexture(MipDim, SK::Graphics::TextureFormat::Red_32, { SK::Graphics::TextureFlag::Lockable });
				int Stride = 4;
#endif
				auto HeightResource = SKRenderer.LockTexture(HeightInitTexture, 0);
				uint8_t* HeightInitData = static_cast<uint8_t*>(HeightResource.Data);
				memset(HeightInitData, 0, HeightResource.DepthPitch);
				SKRenderer.UnlockTexture(HeightInitTexture, 0);
				SKRenderer.CopyTexture(HeightInitTexture, MipHeightTexture);
			}

			UpdateHeightMap(MipDim, MipHeightTexture, MipNormalTexture);

			//Store this result for the next mip
			PrevHeightTex = MipHeightTexture;
		}

		std::cout << "Rendering 100.00%" << std::endl;

		std::cout << "Saving file: " << outFile.GetSystemPath() << std::endl;

		if (outputRaw)
		{
			//Write out texture data to in straight to disk
			SK::IO::MemoryStream Buffer;
			Buffer.Endianness(SK::IO::Endian::Big);
			Buffer.Length(PrevHeightTex->Size().x * PrevHeightTex->Size().y * 2);
			SaveTexture(Buffer, PrevHeightTex);

			SK::IO::FileStream HeightMapFile(outFile, { SK::IO::FileMode::Truncate, SK::IO::FileMode::Read });
			if (HeightMapFile.IsOpen())
			{
				Buffer.CopyTo(HeightMapFile);
			}
			else
			{
				std::cout << "Could not open file for saving: " << outFile.GetSystemPath() << std::endl;
			}
		}
		else
		{
			//Write out texture data to in memory buffer
			SK::IO::MemoryStream Buffer;
			Buffer.Endianness(SK::IO::Endian::Big);
			Buffer.Length(PrevHeightTex->Size().x * PrevHeightTex->Size().y * 2);
			SaveTexture(Buffer, PrevHeightTex);

			//Save as PNG
			lodepng::State PngState;
			PngState.info_raw.bitdepth = 16;
			PngState.info_png.color.bitdepth = 16;
			PngState.info_raw.colortype = LCT_GREY;
			PngState.info_png.color.colortype = LCT_GREY;

			SK::IO::FileStream HeightMapFile(outFile, { SK::IO::FileMode::Truncate, SK::IO::FileMode::Read });
			if (HeightMapFile.IsOpen())
			{
				lodepng::encode(HeightMapFile, Buffer.Data(), PrevHeightTex->Size().x, PrevHeightTex->Size().y, PngState);
				HeightMapFile.Close();
			}
			else
			{
				std::cout << "Could not open file for saving: " << outFile.GetSystemPath() << std::endl;
			}
			Buffer.Close();
		}

		//Normalize and copy out
#ifdef DO_CPU_VER

		floattype Range = HeightMax - HeightMin;
		SK::IO::FileStream HeightMapFile("D:\\GenDisplacement_SK.png", { SK::IO::FileMode::Truncate, SK::IO::FileMode::Read });
		SK::IO::MemoryStream Buffer;
		Buffer.Endianness(SK::IO::Endian::Big);
		Buffer.Length(PrevHeightTex->Size().x * PrevHeightTex->Size().y * 2);
		for (int y = 0; y < PrevHeightTex->Size().y; ++y)
		{
			for (int x = 0; x < PrevHeightTex->Size().x; ++x)
			{
				floattype val = HeightMap[(y*PrevHeightTex->Size().x) + x];
				val -= HeightMin;
				if (NormaliseHeight)
				{
					val /= Range;
					val *= 65535.0f;
				}
				else
				{
					val *= static_cast<floattype>(65535.0 / sqrt((PrevHeightTex->Size().x*PrevHeightTex->Size().x) + (PrevHeightTex->Size().y*PrevHeightTex->Size().y))) * Scale;
					val = std::min(val, floattype(65535.0));
				}

				uint16_t val16 = static_cast<uint16_t>(val);
				Buffer.Write(val16);
			}
		}

		lodepng::State PngState;
		PngState.info_raw.bitdepth = 16;
		PngState.info_png.color.bitdepth = 16;
		PngState.info_raw.colortype = LCT_GREY;
		PngState.info_png.color.colortype = LCT_GREY;

		lodepng::encode(HeightMapFile, Buffer.Data(), PrevHeightTex->Size().x, PrevHeightTex->Size().y, PngState);

		Buffer.Close();
		HeightMapFile.Close();
#endif

	}

	void SaveTexture(SK::IO::Stream& stream, SK::Graphics::TexturePtr texture)
	{
		if (!texture)
			return;

		SKRenderer.SetDefaultRenderTarget();

		SK::Graphics::TexturePtr lockTex = nullptr;
		if (texture->Flags().HasFlags(SK::Graphics::TextureFlag::Staging))
		{
			lockTex = texture;
		}
		else
		{
			lockTex = SKRenderer.CreateTexture(texture->Size(), texture->Format(), SK::Graphics::TextureFlag::Staging);
			SKRenderer.CopyTexture(texture, lockTex);
		}

		auto TexResource = SKRenderer.LockTexture(lockTex, 0);
		uint8_t* TexData = static_cast<uint8_t*>(TexResource.Data);

		//Normalize and copy out
		if (texture->Format() == SK::Graphics::TextureFormat::Red_32)
		{
			float HeightMin = FLT_MAX;
			float HeightMax = -FLT_MAX;
			for (int y = 0; y < texture->Size().y; ++y)
			{
				float* RowBuffer = reinterpret_cast<float*>(TexData + (y * TexResource.RowPitch));

				for (int x = 0; x < texture->Size().x; ++x)
				{
					float val = RowBuffer[x];
					if (val > HeightMax)
						HeightMax = val;

					if (val < HeightMin)
						HeightMin = val;
				}
			}

			float Range = HeightMax - HeightMin;
			for (int y = 0; y < texture->Size().y; ++y)
			{
				float* RowBuffer = reinterpret_cast<float*>(TexData + (y * TexResource.RowPitch));

				for (int x = 0; x < texture->Size().x; ++x)
				{
					float val = RowBuffer[x];
					val -= HeightMin;
					if (NormaliseHeight || Range > 65535.0f)
					{
						val /= Range;
						val *= 65535.0f;

						val = std::min(65535.0f, std::max(0.0f, val));
					}
					else
					{
						val *= static_cast<float>(65535.0 / sqrt((texture->Size().x*texture->Size().x) + (texture->Size().y*texture->Size().y))) * Scale;
						val = std::min(val, 65535.0f);
					}

					uint16_t val16 = static_cast<uint16_t>(val);
					stream.Write(val16);
				}
			}
		}
		else if (texture->Format() == SK::Graphics::TextureFormat::RG_UINT64)
		{
			double* Buffer = reinterpret_cast<double*>(TexData);

			double HeightMin = DBL_MAX;
			double HeightMax = -DBL_MAX;
			for (int y = 0; y < texture->Size().y; ++y)
			{
				for (int x = 0; x < texture->Size().x; ++x)
				{
					double val = Buffer[(y*texture->Size().x) + x];
					if (val > HeightMax)
						HeightMax = val;

					if (val < HeightMin)
						HeightMin = val;
				}
			}

			double Range = HeightMax - HeightMin;
			for (int y = 0; y < texture->Size().y; ++y)
			{
				for (int x = 0; x < texture->Size().x; ++x)
				{
					double val = Buffer[(y*texture->Size().x) + x];
					if (NormaliseHeight || Range > 65535.0)
					{
						val -= HeightMin;
						val /= Range;
						val *= 65535.0;
					}
					else
					{
						val -= HeightMin;
						val *= 65535.0 / sqrt((texture->Size().x*texture->Size().x) + (texture->Size().y*texture->Size().y)) * Scale;
						val = std::min(val, 65535.0);
					}

					uint16_t val16 = static_cast<uint16_t>(val);
					stream.Write(val16);
				}
			}
		}
		else
		{
			stream.WriteData(TexData, 0, texture->Size().x * texture->Size().y * SK::Graphics::TextureSampleSize(texture->Format()));
		}

		SKRenderer.UnlockTexture(lockTex, 0);
	}

	float GetHeightFromAngle(float angleDot)
	{
		const float MaxDelta = 30.0f;
		float res = 0.0f;
		if (angleDot > -1.0 && angleDot < 1.0)
		{
			res = tan(asin(angleDot));
			res = std::max(std::min(res, MaxDelta), -MaxDelta);
		}
		else
		{
			res = angleDot < 0 ? -MaxDelta : MaxDelta;
		};

		return res;
	}

	SK::Math::float3 GetNormal(const uint16_t* colour)
	{
		SK::Math::float3 Normal;
		Normal.x = (DirectX::PackedVector::XMConvertHalfToFloat(colour[0]) * 2.0f) - 1.0f;
		Normal.y = (DirectX::PackedVector::XMConvertHalfToFloat(colour[1]) * 2.0f) - 1.0f;
		Normal.z = (DirectX::PackedVector::XMConvertHalfToFloat(colour[2]) * 2.0f) - 1.0f;

		float len = sqrt((Normal.x * Normal.x) + (Normal.y * Normal.y) + (Normal.z * Normal.z));
		Normal.x /= len;
		Normal.y /= len;
		Normal.z /= len;

		return Normal;
	}

	float GetPixelDelta(const uint16_t* normalMap, int stride, SK::Math::int2 dim, int x, int y, PixelDir dir)
	{
		SK::Math::float3 VecDir(0.0f);
		int PixelId = (y * dim.x) + x;
		SK::Math::float3 Normal = GetNormal(&normalMap[PixelId * stride]);
		switch (dir)
		{
		case PixelDir::North:
			VecDir = SK::Math::float3(0.0f, -1.0f, 0.0f);
			break;
		case PixelDir::NorthEast:
			VecDir = SK::Math::float3(-1.0f, -1.0f, 0.0f);
			break;
		case PixelDir::East:
			VecDir = SK::Math::float3(-1.0, 0.0f, 0.0f);
			break;
		case PixelDir::SouthEast:
			VecDir = SK::Math::float3(-1.0f, 1.0f, 0.0f);
			break;
		case PixelDir::South:
			VecDir = SK::Math::float3(0.0f, 1.0f, 0.0f);
			break;
		case PixelDir::SouthWest:
			VecDir = SK::Math::float3(1.0f, 1.0f, 0.0f);
			break;
		case PixelDir::West:
			VecDir = SK::Math::float3(1.0f, 0.0f, 0.0f);
			break;
		case PixelDir::NorthWest:
			VecDir = SK::Math::float3(1.0f, -1.0f, 0.0f);
			break;
		}

		const float Dist = (Normal.x * VecDir.x) + (Normal.y * VecDir.y);
		const float AngDot = sqrt((Normal.y * Normal.y) + (Normal.x * Normal.x));
		if (AngDot > 0)
			return GetHeightFromAngle(AngDot) * (Dist / AngDot);

		return 0.0f;
	}

	void UpdateHeightMap(const SK::Math::int2& Dim, SK::Graphics::TexturePtr HeightMapTexture, SK::Graphics::TexturePtr NormalMapTexture)
	{
		SK::Graphics::TexturePtr DeltasA = SKRenderer.CreateTexture(Dim, SK::Graphics::TextureFormat::RGBA_UINT128, SK::Graphics::TextureFlag::RenderTarget);
		SK::Graphics::TexturePtr DeltasB = SKRenderer.CreateTexture(Dim, SK::Graphics::TextureFormat::RGBA_UINT128, SK::Graphics::TextureFlag::RenderTarget);
		SK::Graphics::TexturePtr DeltasC = SKRenderer.CreateTexture(Dim, SK::Graphics::TextureFormat::RGBA_UINT128, SK::Graphics::TextureFlag::RenderTarget);

		//Init deltas
		SKRenderer.SetPixelShader(PShaderGenerateDeltas);
		SKRenderer.SetBlendingMode(SK::Graphics::BlendMode::None);
		SKRenderer.SetShaderTexture(4, NormalMapTexture);
		SK::Graphics::TexturePtr RTS[3] = { DeltasA, DeltasB, DeltasC };
		int Faces[3] = { 0, 0, 0 };
		SKRenderer.SetRenderTargets(RTS, Faces, 3, nullptr);
		size_t StepHeightParam = SKRenderer.GetShaderParamId("MaxStepHeight");
		SKRenderer.SetShaderParam(StepHeightParam, &MaxStepHeight, sizeof(MaxStepHeight));
		SKRenderer.CommitShaderParams();
		SKRenderer.DrawBuffer(SK::Graphics::PrimType::TriangleStrip, FullscreenQuad, 4, 0);
		SKRenderer.SetPixelShader(PShaderUpdateHeights);

		SK::Graphics::TexturePtr HeightMapTextureAlt = SKRenderer.CreateTexture(HeightMapTexture->Size(), HeightMapTexture->Format(), SK::Graphics::TextureFlag::RenderTarget);

		bool RenderingToAlt = true;

		uint32_t Direction = 0;
		SKRenderer.SetRenderTarget(HeightMapTextureAlt, 0, nullptr);
		SKRenderer.SetShaderTexture(0, HeightMapTexture);
		SKRenderer.SetShaderTexture(1, DeltasA);
		SKRenderer.SetShaderTexture(2, DeltasB);
		SKRenderer.SetShaderTexture(3, DeltasC);
		SKRenderer.CommitShaderParams();

		for (int pass = 0; pass < NumPasses; ++pass)
		{
			SKRenderer.DrawBuffer(SK::Graphics::PrimType::TriangleStrip, FullscreenQuad, 4, 0);

			SKRenderer.SetShaderTexture(0, nullptr);
			SKRenderer.CommitShaderParams();

			if (RenderingToAlt)
			{
				SKRenderer.SetRenderTarget(HeightMapTexture, 0, nullptr);
				SKRenderer.SetShaderTexture(0, HeightMapTextureAlt);
				SKRenderer.CommitShaderParams();

				RenderingToAlt = false;
			}
			else
			{
				SKRenderer.SetRenderTarget(HeightMapTextureAlt, 0, nullptr);
				SKRenderer.SetShaderTexture(0, HeightMapTexture);
				SKRenderer.CommitShaderParams();
				RenderingToAlt = true;
			}
			SKRenderer.Flush();
			SK::Threading::Sleep(SK::Time::Milliseconds(1));

			++NumPassesComplete;
			//Update every third a percent(ish)
			bool UpdatePercentage = TotalNumPasses > 300 ? (NumPassesComplete % (TotalNumPasses / 300)) == 0 : true;
			if (UpdatePercentage)
			{
				double PercentageComplete = static_cast<double>(NumPassesComplete) / TotalNumPasses;
				std::cout << "Rendering " << std::fixed << std::setprecision(2) << PercentageComplete * 100 << "%\r" << std::flush;
			}
		}

		SKRenderer.Flush();
		SKRenderer.SetDefaultRenderTarget();
		if (!RenderingToAlt)
			SKRenderer.CopyTexture(HeightMapTextureAlt, HeightMapTexture);
	}
//-----------------------------------------------------------------------------
} //namespace NormalToHeight
//-----------------------------------------------------------------------------

void NormalMapTest(int numArgs, SK::String args[])
{
	SK::String InFile;
	SK::String MaskFile = "";
	SK::String OutFile;
	bool HaveInFile = false;
	bool HaveOutFile = false;

	bool Normalise = false;
	float Scale = 1.0f;
	float NormalScale = 1.0f;
	float MaxStepHeight = 50.0f;
	int NumPasses = 2048;
	NormalToHeight::EEdgeMode EdgeMode = NormalToHeight::EEdgeMode::Free;
	
	bool RawOutput = false;
	NormalToHeight::ChannelMapping Mapping;
	Mapping.X = NormalToHeight::EChannel::Red;
	Mapping.Y = NormalToHeight::EChannel::Green;
	Mapping.Z = NormalToHeight::EChannel::Blue;

	int arg = 0;
	while (arg < numArgs)
	{
		if (args[arg].EqualsCI("-scale"))
		{
			if (++arg < numArgs)
				Scale = std::stof((char*)args[arg].Buffer());
		}
		else if (args[arg].EqualsCI("-normalScale"))
		{
			if (++arg < numArgs)
				NormalScale = std::stof((char*)args[arg].Buffer());
		}
		else if (args[arg].EqualsCI("-maxStepHeight"))
		{
			if (++arg < numArgs)
				MaxStepHeight = abs(std::stof((char*)args[arg].Buffer()));
		}
		else if (args[arg].EqualsCI("-normalise") || args[arg].EqualsCI("-normalize"))
		{
			Normalise = true;
		}
		else if (args[arg].EqualsCI("-OutputRaw"))
		{
			RawOutput = true;
		}
		else if (args[arg].EqualsCI("-numPasses"))
		{
			if (++arg < numArgs)
				NumPasses = std::stoi((char*)args[arg].Buffer());
		}
		else if (args[arg].EqualsCI("-mask"))
		{
			if (++arg < numArgs)
			{
				MaskFile = args[arg];
			}
		}
		else if (args[arg].EqualsCI("-mapping"))
		{
			if (++arg < numArgs)
			{
				SK::String MappingStr = args[arg];

				NormalToHeight::EChannel* TargetChannel = &Mapping.X;
				bool FlipChannel = false;
				for (SK::CodePoint ch : MappingStr)
				{
					switch (ch)
					{
					case 'x':
					case 'X':
						TargetChannel = &Mapping.X;
						FlipChannel = false;
						break;

					case 'y':
					case 'Y':
						TargetChannel = &Mapping.Y;
						FlipChannel = false;
						break;

					case 'z':
					case 'Z':
						TargetChannel = &Mapping.Z;
						FlipChannel = false;
						break;

					case 'f':
					case 'F':
						FlipChannel = true;
						break;

					case 'r':
					case 'R':
						*TargetChannel = FlipChannel ? NormalToHeight::EChannel::OneMinus_Red : NormalToHeight::EChannel::Red;
						break;

					case 'g':
					case 'G':
						*TargetChannel = FlipChannel ? NormalToHeight::EChannel::OneMinus_Green : NormalToHeight::EChannel::Green;
						break;

					case 'b':
					case 'B':
						*TargetChannel = FlipChannel ? NormalToHeight::EChannel::OneMinus_Blue : NormalToHeight::EChannel::Blue;
						break;

					case 'a':
					case 'A':
						*TargetChannel = FlipChannel ? NormalToHeight::EChannel::OneMinus_Alpha : NormalToHeight::EChannel::Alpha;
						break;

					case 'n':
					case 'N':
						*TargetChannel = NormalToHeight::EChannel::None;
						break;
					}
				}
			}
		}
		else if (args[arg].EqualsCI("-zrange"))
		{
			SK::String Range = args[++arg];
			if (Range.EqualsCI("full"))
			{
				Mapping.ZRange = NormalToHeight::EZRange::MinusOne_to_One;
			}
			else if (Range.EqualsCI("half"))
			{
				Mapping.ZRange = NormalToHeight::EZRange::Zero_to_One;
			}
			else if (Range.EqualsCI("clamped"))
			{
				Mapping.ZRange = NormalToHeight::EZRange::Clamp_Zero_to_One;
			}
		}
		else if (args[arg].EqualsCI("-edgeMode"))
		{
			SK::String Mode = args[++arg];
			if (Mode.EqualsCI("Free"))
			{
				EdgeMode = NormalToHeight::EEdgeMode::Free;
			}
			else if (Mode.EqualsCI("Clamp"))
			{
				EdgeMode = NormalToHeight::EEdgeMode::Clamp;
			}
			else if (Mode.EqualsCI("Wrap"))
			{
				EdgeMode = NormalToHeight::EEdgeMode::Wrap;
			}
		}
		else
		{
			if (!HaveInFile)
			{
				InFile = args[arg];
				HaveInFile = true;
			}
			else if (!HaveOutFile)
			{
				OutFile = args[arg];
				HaveOutFile = true;
			}
		}
		++arg;
	}

	if (numArgs < 2 || !HaveInFile || !HaveOutFile)
	{
		std::cout << "Usage: inFile outFile [-normalise] [-scale x.xx] [-numPasses x] [-normalScale x.xx] [-maxStepHeight x.xx] [-mask maskFile] [-mapping xxxxxx] [-zrange full/half/clamped]" << std::endl;
		std::cout << " inFile - Normal Map input" << std::endl;
		std::cout << " outFile - Displacement Map output" << std::endl;
		std::cout << " normalise - Normalise displacement map output (lowest value is black, highest value is white)" << std::endl;
		std::cout << " scale - Scale displacement map heights (when normalise is not selected)" << std::endl;
		std::cout << " numPasses - Number of passes to perform, more passes produces a more accurate result, but takes longer" << std::endl;
		std::cout << " normalScale - Increase or decrease the 'strength' of the normal map (can be negative)" << std::endl;
		std::cout << " maxStepHeight - Maximum displacement between two adjacent pixels" << std::endl;
		std::cout << " mask - Optional black and white image, all black areas will have a height of 0 in the displacement map" << std::endl;
		std::cout << " mapping - Map XYZ normal components to colour channels, for example:" << std::endl;
		std::cout << "           XrYgZb - X is red, Y is green, Z is blue" << std::endl;
		std::cout << "           XgYaZn - X is green, Y is alpha, Z is not present" << std::endl;
		std::cout << "           XrYfgZn - X is red, Y is green (but the direction is flipped), Z is not present" << std::endl;
		std::cout << " zrange - Z component range:" << std::endl;
		std::cout << "          Full - -1.0 - 1.0" << std::endl;
		std::cout << "          Half -  0.0 - 1.0" << std::endl;
		std::cout << "          Clamped - Full range, but clamped to 0.0" << std::endl;
		std::cout << " edgeMode - How to handle displacement map edges:" << std::endl;
		std::cout << "          Free - (Default) Reference points beyond the edge of the map are ignored, edges can be any height" << std::endl;
		std::cout << "          Clamp - Reference points beyond the edge of the map are treated as Zero" << std::endl;
		std::cout << "          Wrap - The map is a repeating pattern, reference points wrap to the other side of the map" << std::endl;
		return;
	}

	SK::IO::Path InPath(SK::IO::SpecialFolder::None);
	if (SK::IO::Path::IsRelative(InFile))
	{
		wchar_t DirBuff[1024];
		GetCurrentDirectory(1024, DirBuff);
		InPath = SK::IO::Path::Combine(SK::Encoding::UTF16::FromBytes(reinterpret_cast<uint8_t*>(DirBuff)), InFile);
	}
	else
	{
		InPath = SK::IO::Path(SK::IO::SpecialFolder::None, InFile);
	}

	SK::IO::Path OutPath(SK::IO::SpecialFolder::None);
	if (SK::IO::Path::IsRelative(OutFile))
	{
		wchar_t DirBuff[1024];
		GetCurrentDirectory(1024, DirBuff);
		OutPath = SK::IO::Path::Combine(SK::Encoding::UTF16::FromBytes(reinterpret_cast<uint8_t*>(DirBuff)), OutFile);
	}
	else
	{
		OutPath = SK::IO::Path(SK::IO::SpecialFolder::None, OutFile);
	}

	SK::IO::Path MaskPath(SK::IO::SpecialFolder::None);
	if (SK::IO::Path::IsRelative(MaskFile))
	{
		wchar_t DirBuff[1024];
		GetCurrentDirectory(1024, DirBuff);
		MaskPath = SK::IO::Path::Combine(SK::Encoding::UTF16::FromBytes(reinterpret_cast<uint8_t*>(DirBuff)), MaskFile);
	}
	else
	{
		MaskPath = SK::IO::Path(SK::IO::SpecialFolder::None, MaskFile);
	}

	if (!SK::IO::File::Exists(InPath))
	{
		std::cout << InPath.GetSystemPath() << " does not exist" << std::endl;
		return;
	}

	NormalToHeight::Convert(InPath, OutPath, MaskPath, NumPasses, Normalise, Scale, NormalScale, MaxStepHeight, EdgeMode, Mapping, RawOutput);
}

int _tmain(int argc, _TCHAR* argv[])
{
	SK::String* Args = new SK::String[argc];
	for (int i = 1; i < argc; ++i)
		Args[i - 1] = SK::Encoding::UTF16::FromBytes(reinterpret_cast<uint8_t*>(argv[i]));

	NormalMapTest(argc - 1, Args);

	delete[] Args;

	return 0;
}