﻿namespace NormalToHeightGUI
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.inFile_TextBox = new System.Windows.Forms.TextBox();
			this.inFileBrowse_Button = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.maskFile_TextBox = new System.Windows.Forms.TextBox();
			this.maskFileBrowse_Button = new System.Windows.Forms.Button();
			this.normalMap_PictureBox = new System.Windows.Forms.PictureBox();
			this.normalise_CheckBox = new System.Windows.Forms.CheckBox();
			this.scale_NumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.label4 = new System.Windows.Forms.Label();
			this.numPasses_NumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.normalScale_NumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.label7 = new System.Windows.Forms.Label();
			this.maxStepHeight_NumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.mappingX_ComboBox = new System.Windows.Forms.ComboBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.mappingY_ComboBox = new System.Windows.Forms.ComboBox();
			this.label10 = new System.Windows.Forms.Label();
			this.mappingZ_ComboBox = new System.Windows.Forms.ComboBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.edgeMode_ComboBox = new System.Windows.Forms.ComboBox();
			this.label11 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.label2 = new System.Windows.Forms.Label();
			this.zrange_ComboBox = new System.Windows.Forms.ComboBox();
			this.preview_Button = new System.Windows.Forms.Button();
			this.save_Button = new System.Windows.Forms.Button();
			this.openImageDialog = new System.Windows.Forms.OpenFileDialog();
			this.infoToolTip = new System.Windows.Forms.ToolTip(this.components);
			this.statusStrip = new System.Windows.Forms.StatusStrip();
			this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.progressBar = new System.Windows.Forms.ToolStripProgressBar();
			this.preview_TabControl = new System.Windows.Forms.TabControl();
			this.normalMap_TabPage = new System.Windows.Forms.TabPage();
			this.mask_TabPage = new System.Windows.Forms.TabPage();
			this.mask_PictureBox = new System.Windows.Forms.PictureBox();
			this.preview_TabPage = new System.Windows.Forms.TabPage();
			this.preview_PictureBox = new System.Windows.Forms.PictureBox();
			this.saveDisplacementMapDialog = new System.Windows.Forms.SaveFileDialog();
			((System.ComponentModel.ISupportInitialize)(this.normalMap_PictureBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.scale_NumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numPasses_NumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.normalScale_NumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.maxStepHeight_NumericUpDown)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.statusStrip.SuspendLayout();
			this.preview_TabControl.SuspendLayout();
			this.normalMap_TabPage.SuspendLayout();
			this.mask_TabPage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.mask_PictureBox)).BeginInit();
			this.preview_TabPage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.preview_PictureBox)).BeginInit();
			this.SuspendLayout();
			// 
			// inFile_TextBox
			// 
			this.inFile_TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.inFile_TextBox.Location = new System.Drawing.Point(12, 25);
			this.inFile_TextBox.Name = "inFile_TextBox";
			this.inFile_TextBox.Size = new System.Drawing.Size(371, 20);
			this.inFile_TextBox.TabIndex = 0;
			this.inFile_TextBox.TextChanged += new System.EventHandler(this.inFile_TextBox_TextChanged);
			// 
			// inFileBrowse_Button
			// 
			this.inFileBrowse_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.inFileBrowse_Button.Location = new System.Drawing.Point(389, 25);
			this.inFileBrowse_Button.Name = "inFileBrowse_Button";
			this.inFileBrowse_Button.Size = new System.Drawing.Size(31, 20);
			this.inFileBrowse_Button.TabIndex = 2;
			this.inFileBrowse_Button.Text = "...";
			this.infoToolTip.SetToolTip(this.inFileBrowse_Button, "Browse");
			this.inFileBrowse_Button.UseVisualStyleBackColor = true;
			this.inFileBrowse_Button.Click += new System.EventHandler(this.inFileBrowse_Button_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 13);
			this.label1.TabIndex = 4;
			this.label1.Text = "Normal Map";
			this.infoToolTip.SetToolTip(this.label1, "The normal map to process");
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 48);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(33, 13);
			this.label3.TabIndex = 6;
			this.label3.Text = "Mask";
			this.infoToolTip.SetToolTip(this.label3, "Optional mask.\r\nOnly the white areas of the mask will be processed for the normal" +
        " map, black areas will have a height of 0");
			// 
			// maskFile_TextBox
			// 
			this.maskFile_TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.maskFile_TextBox.Location = new System.Drawing.Point(12, 65);
			this.maskFile_TextBox.Name = "maskFile_TextBox";
			this.maskFile_TextBox.Size = new System.Drawing.Size(371, 20);
			this.maskFile_TextBox.TabIndex = 7;
			this.maskFile_TextBox.TextChanged += new System.EventHandler(this.maskFile_TextBox_TextChanged);
			// 
			// maskFileBrowse_Button
			// 
			this.maskFileBrowse_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.maskFileBrowse_Button.Location = new System.Drawing.Point(389, 65);
			this.maskFileBrowse_Button.Name = "maskFileBrowse_Button";
			this.maskFileBrowse_Button.Size = new System.Drawing.Size(31, 20);
			this.maskFileBrowse_Button.TabIndex = 8;
			this.maskFileBrowse_Button.Text = "...";
			this.infoToolTip.SetToolTip(this.maskFileBrowse_Button, "Browse");
			this.maskFileBrowse_Button.UseVisualStyleBackColor = true;
			this.maskFileBrowse_Button.Click += new System.EventHandler(this.maskFileBrowse_Button_Click);
			// 
			// normalMap_PictureBox
			// 
			this.normalMap_PictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.normalMap_PictureBox.BackColor = System.Drawing.Color.Black;
			this.normalMap_PictureBox.Location = new System.Drawing.Point(0, 0);
			this.normalMap_PictureBox.Name = "normalMap_PictureBox";
			this.normalMap_PictureBox.Size = new System.Drawing.Size(400, 400);
			this.normalMap_PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.normalMap_PictureBox.TabIndex = 9;
			this.normalMap_PictureBox.TabStop = false;
			// 
			// normalise_CheckBox
			// 
			this.normalise_CheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.normalise_CheckBox.Location = new System.Drawing.Point(12, 162);
			this.normalise_CheckBox.Name = "normalise_CheckBox";
			this.normalise_CheckBox.Size = new System.Drawing.Size(113, 17);
			this.normalise_CheckBox.TabIndex = 11;
			this.normalise_CheckBox.Text = "Normalise";
			this.infoToolTip.SetToolTip(this.normalise_CheckBox, "Normalise displacement map output (lowest value is black, highest value is white)" +
        "");
			this.normalise_CheckBox.UseVisualStyleBackColor = true;
			this.normalise_CheckBox.CheckedChanged += new System.EventHandler(this.normalise_CheckBox_CheckedChanged);
			// 
			// scale_NumericUpDown
			// 
			this.scale_NumericUpDown.DecimalPlaces = 2;
			this.scale_NumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.scale_NumericUpDown.Location = new System.Drawing.Point(111, 30);
			this.scale_NumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.scale_NumericUpDown.Name = "scale_NumericUpDown";
			this.scale_NumericUpDown.Size = new System.Drawing.Size(120, 20);
			this.scale_NumericUpDown.TabIndex = 12;
			this.scale_NumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(12, 32);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(34, 13);
			this.label4.TabIndex = 13;
			this.label4.Text = "Scale";
			this.infoToolTip.SetToolTip(this.label4, "Scales the height of the generated displacement map (when Normalise is not selece" +
        "td)");
			// 
			// numPasses_NumericUpDown
			// 
			this.numPasses_NumericUpDown.Location = new System.Drawing.Point(111, 56);
			this.numPasses_NumericUpDown.Maximum = new decimal(new int[] {
            16384,
            0,
            0,
            0});
			this.numPasses_NumericUpDown.Name = "numPasses_NumericUpDown";
			this.numPasses_NumericUpDown.Size = new System.Drawing.Size(120, 20);
			this.numPasses_NumericUpDown.TabIndex = 14;
			this.numPasses_NumericUpDown.Value = new decimal(new int[] {
            4096,
            0,
            0,
            0});
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(12, 58);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(93, 13);
			this.label5.TabIndex = 15;
			this.label5.Text = "Number of Passes";
			this.infoToolTip.SetToolTip(this.label5, "Number of passes to perform, more passes produces a more accurate result, but tak" +
        "es longer");
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(12, 84);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(70, 13);
			this.label6.TabIndex = 17;
			this.label6.Text = "Normal Scale";
			this.infoToolTip.SetToolTip(this.label6, "Increase or decrease the \'strength\' of the normal map");
			// 
			// normalScale_NumericUpDown
			// 
			this.normalScale_NumericUpDown.DecimalPlaces = 3;
			this.normalScale_NumericUpDown.Location = new System.Drawing.Point(111, 82);
			this.normalScale_NumericUpDown.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
			this.normalScale_NumericUpDown.Name = "normalScale_NumericUpDown";
			this.normalScale_NumericUpDown.Size = new System.Drawing.Size(120, 20);
			this.normalScale_NumericUpDown.TabIndex = 16;
			this.normalScale_NumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(12, 110);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(86, 13);
			this.label7.TabIndex = 19;
			this.label7.Text = "Max Step Height";
			this.infoToolTip.SetToolTip(this.label7, "Maximum displacement between two adjacent pixels");
			// 
			// maxStepHeight_NumericUpDown
			// 
			this.maxStepHeight_NumericUpDown.Location = new System.Drawing.Point(111, 108);
			this.maxStepHeight_NumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.maxStepHeight_NumericUpDown.Name = "maxStepHeight_NumericUpDown";
			this.maxStepHeight_NumericUpDown.Size = new System.Drawing.Size(120, 20);
			this.maxStepHeight_NumericUpDown.TabIndex = 18;
			this.maxStepHeight_NumericUpDown.Value = new decimal(new int[] {
            75,
            0,
            0,
            0});
			// 
			// mappingX_ComboBox
			// 
			this.mappingX_ComboBox.FormattingEnabled = true;
			this.mappingX_ComboBox.Location = new System.Drawing.Point(32, 19);
			this.mappingX_ComboBox.Name = "mappingX_ComboBox";
			this.mappingX_ComboBox.Size = new System.Drawing.Size(121, 21);
			this.mappingX_ComboBox.TabIndex = 20;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(12, 22);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(14, 13);
			this.label8.TabIndex = 21;
			this.label8.Text = "X";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(12, 49);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(14, 13);
			this.label9.TabIndex = 23;
			this.label9.Text = "Y";
			// 
			// mappingY_ComboBox
			// 
			this.mappingY_ComboBox.FormattingEnabled = true;
			this.mappingY_ComboBox.Location = new System.Drawing.Point(32, 46);
			this.mappingY_ComboBox.Name = "mappingY_ComboBox";
			this.mappingY_ComboBox.Size = new System.Drawing.Size(121, 21);
			this.mappingY_ComboBox.TabIndex = 22;
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(12, 76);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(14, 13);
			this.label10.TabIndex = 25;
			this.label10.Text = "Z";
			// 
			// mappingZ_ComboBox
			// 
			this.mappingZ_ComboBox.FormattingEnabled = true;
			this.mappingZ_ComboBox.Location = new System.Drawing.Point(32, 73);
			this.mappingZ_ComboBox.Name = "mappingZ_ComboBox";
			this.mappingZ_ComboBox.Size = new System.Drawing.Size(121, 21);
			this.mappingZ_ComboBox.TabIndex = 24;
			this.mappingZ_ComboBox.SelectedValueChanged += new System.EventHandler(this.mappingZ_ComboBox_SelectedValueChanged);
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.edgeMode_ComboBox);
			this.groupBox1.Controls.Add(this.label11);
			this.groupBox1.Controls.Add(this.groupBox2);
			this.groupBox1.Controls.Add(this.normalise_CheckBox);
			this.groupBox1.Controls.Add(this.label7);
			this.groupBox1.Controls.Add(this.scale_NumericUpDown);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.numPasses_NumericUpDown);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.normalScale_NumericUpDown);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.maxStepHeight_NumericUpDown);
			this.groupBox1.Location = new System.Drawing.Point(439, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(244, 342);
			this.groupBox1.TabIndex = 26;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Settings";
			// 
			// edgeMode_ComboBox
			// 
			this.edgeMode_ComboBox.FormattingEnabled = true;
			this.edgeMode_ComboBox.Location = new System.Drawing.Point(111, 134);
			this.edgeMode_ComboBox.Name = "edgeMode_ComboBox";
			this.edgeMode_ComboBox.Size = new System.Drawing.Size(86, 21);
			this.edgeMode_ComboBox.TabIndex = 29;
			this.infoToolTip.SetToolTip(this.edgeMode_ComboBox, resources.GetString("edgeMode_ComboBox.ToolTip"));
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(12, 136);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(62, 13);
			this.label11.TabIndex = 28;
			this.label11.Text = "Edge Mode";
			this.infoToolTip.SetToolTip(this.label11, "How to handle the displacement map edges");
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.label2);
			this.groupBox2.Controls.Add(this.zrange_ComboBox);
			this.groupBox2.Controls.Add(this.mappingX_ComboBox);
			this.groupBox2.Controls.Add(this.label8);
			this.groupBox2.Controls.Add(this.label10);
			this.groupBox2.Controls.Add(this.mappingY_ComboBox);
			this.groupBox2.Controls.Add(this.mappingZ_ComboBox);
			this.groupBox2.Controls.Add(this.label9);
			this.groupBox2.Location = new System.Drawing.Point(15, 195);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(168, 135);
			this.groupBox2.TabIndex = 27;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Mapping";
			this.infoToolTip.SetToolTip(this.groupBox2, "Map XYZ normal components to colour channels");
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 103);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(49, 13);
			this.label2.TabIndex = 27;
			this.label2.Text = "Z-Range";
			// 
			// zrange_ComboBox
			// 
			this.zrange_ComboBox.FormattingEnabled = true;
			this.zrange_ComboBox.Location = new System.Drawing.Point(67, 100);
			this.zrange_ComboBox.Name = "zrange_ComboBox";
			this.zrange_ComboBox.Size = new System.Drawing.Size(86, 21);
			this.zrange_ComboBox.TabIndex = 26;
			this.infoToolTip.SetToolTip(this.zrange_ComboBox, "Full: -1.0 - 1.0\r\nHalf: 0.0 - 1.0\r\nClamped: Full range, clamped to 0.0 - 1.0");
			// 
			// preview_Button
			// 
			this.preview_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.preview_Button.Enabled = false;
			this.preview_Button.Location = new System.Drawing.Point(527, 360);
			this.preview_Button.Name = "preview_Button";
			this.preview_Button.Size = new System.Drawing.Size(75, 23);
			this.preview_Button.TabIndex = 27;
			this.preview_Button.Text = "Preview";
			this.preview_Button.UseVisualStyleBackColor = true;
			this.preview_Button.Click += new System.EventHandler(this.preview_Button_Click);
			// 
			// save_Button
			// 
			this.save_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.save_Button.Enabled = false;
			this.save_Button.Location = new System.Drawing.Point(608, 360);
			this.save_Button.Name = "save_Button";
			this.save_Button.Size = new System.Drawing.Size(75, 23);
			this.save_Button.TabIndex = 28;
			this.save_Button.Text = "Save";
			this.save_Button.UseVisualStyleBackColor = true;
			this.save_Button.Click += new System.EventHandler(this.save_Button_Click);
			// 
			// openImageDialog
			// 
			this.openImageDialog.AddExtension = false;
			this.openImageDialog.CheckPathExists = false;
			this.openImageDialog.Filter = "Image Files|*.jpg;*.jpeg;*.png;*.bmp;*.gif;*.tiff;*.tif";
			// 
			// statusStrip
			// 
			this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel,
            this.progressBar});
			this.statusStrip.Location = new System.Drawing.Point(0, 527);
			this.statusStrip.Name = "statusStrip";
			this.statusStrip.Size = new System.Drawing.Size(695, 22);
			this.statusStrip.TabIndex = 29;
			this.statusStrip.Text = "statusStrip1";
			// 
			// statusLabel
			// 
			this.statusLabel.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
			this.statusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.statusLabel.Name = "statusLabel";
			this.statusLabel.Padding = new System.Windows.Forms.Padding(0, 0, 8, 0);
			this.statusLabel.Size = new System.Drawing.Size(88, 19);
			this.statusLabel.Text = "Status: Hello!";
			this.statusLabel.Visible = false;
			// 
			// progressBar
			// 
			this.progressBar.Margin = new System.Windows.Forms.Padding(8, 3, 3, 3);
			this.progressBar.Maximum = 200;
			this.progressBar.Name = "progressBar";
			this.progressBar.Size = new System.Drawing.Size(200, 18);
			this.progressBar.Step = 2;
			this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
			this.progressBar.Value = 150;
			this.progressBar.Visible = false;
			// 
			// preview_TabControl
			// 
			this.preview_TabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.preview_TabControl.Controls.Add(this.normalMap_TabPage);
			this.preview_TabControl.Controls.Add(this.mask_TabPage);
			this.preview_TabControl.Controls.Add(this.preview_TabPage);
			this.preview_TabControl.Location = new System.Drawing.Point(12, 98);
			this.preview_TabControl.Name = "preview_TabControl";
			this.preview_TabControl.SelectedIndex = 0;
			this.preview_TabControl.Size = new System.Drawing.Size(408, 426);
			this.preview_TabControl.TabIndex = 30;
			// 
			// normalMap_TabPage
			// 
			this.normalMap_TabPage.Controls.Add(this.normalMap_PictureBox);
			this.normalMap_TabPage.Location = new System.Drawing.Point(4, 22);
			this.normalMap_TabPage.Name = "normalMap_TabPage";
			this.normalMap_TabPage.Padding = new System.Windows.Forms.Padding(3);
			this.normalMap_TabPage.Size = new System.Drawing.Size(400, 400);
			this.normalMap_TabPage.TabIndex = 0;
			this.normalMap_TabPage.Text = "Normal Map";
			this.normalMap_TabPage.UseVisualStyleBackColor = true;
			// 
			// mask_TabPage
			// 
			this.mask_TabPage.Controls.Add(this.mask_PictureBox);
			this.mask_TabPage.Location = new System.Drawing.Point(4, 22);
			this.mask_TabPage.Name = "mask_TabPage";
			this.mask_TabPage.Padding = new System.Windows.Forms.Padding(3);
			this.mask_TabPage.Size = new System.Drawing.Size(400, 400);
			this.mask_TabPage.TabIndex = 1;
			this.mask_TabPage.Text = "Mask";
			this.mask_TabPage.UseVisualStyleBackColor = true;
			// 
			// mask_PictureBox
			// 
			this.mask_PictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.mask_PictureBox.BackColor = System.Drawing.Color.Black;
			this.mask_PictureBox.Location = new System.Drawing.Point(0, 0);
			this.mask_PictureBox.Name = "mask_PictureBox";
			this.mask_PictureBox.Size = new System.Drawing.Size(401, 401);
			this.mask_PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.mask_PictureBox.TabIndex = 10;
			this.mask_PictureBox.TabStop = false;
			// 
			// preview_TabPage
			// 
			this.preview_TabPage.Controls.Add(this.preview_PictureBox);
			this.preview_TabPage.Location = new System.Drawing.Point(4, 22);
			this.preview_TabPage.Name = "preview_TabPage";
			this.preview_TabPage.Size = new System.Drawing.Size(400, 400);
			this.preview_TabPage.TabIndex = 2;
			this.preview_TabPage.Text = "Preview";
			this.preview_TabPage.UseVisualStyleBackColor = true;
			// 
			// preview_PictureBox
			// 
			this.preview_PictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.preview_PictureBox.BackColor = System.Drawing.Color.Black;
			this.preview_PictureBox.Location = new System.Drawing.Point(0, 0);
			this.preview_PictureBox.Name = "preview_PictureBox";
			this.preview_PictureBox.Size = new System.Drawing.Size(400, 400);
			this.preview_PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.preview_PictureBox.TabIndex = 11;
			this.preview_PictureBox.TabStop = false;
			// 
			// saveDisplacementMapDialog
			// 
			this.saveDisplacementMapDialog.Filter = "PNG|*.png;";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(695, 549);
			this.Controls.Add(this.preview_TabControl);
			this.Controls.Add(this.statusStrip);
			this.Controls.Add(this.save_Button);
			this.Controls.Add(this.preview_Button);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.maskFileBrowse_Button);
			this.Controls.Add(this.maskFile_TextBox);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.inFileBrowse_Button);
			this.Controls.Add(this.inFile_TextBox);
			this.MinimumSize = new System.Drawing.Size(518, 395);
			this.Name = "Form1";
			this.Text = "NormalToHeight";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
			((System.ComponentModel.ISupportInitialize)(this.normalMap_PictureBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.scale_NumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numPasses_NumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.normalScale_NumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.maxStepHeight_NumericUpDown)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.statusStrip.ResumeLayout(false);
			this.statusStrip.PerformLayout();
			this.preview_TabControl.ResumeLayout(false);
			this.normalMap_TabPage.ResumeLayout(false);
			this.mask_TabPage.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.mask_PictureBox)).EndInit();
			this.preview_TabPage.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.preview_PictureBox)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox inFile_TextBox;
		private System.Windows.Forms.Button inFileBrowse_Button;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox maskFile_TextBox;
		private System.Windows.Forms.Button maskFileBrowse_Button;
		private System.Windows.Forms.PictureBox normalMap_PictureBox;
		private System.Windows.Forms.CheckBox normalise_CheckBox;
		private System.Windows.Forms.NumericUpDown scale_NumericUpDown;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.NumericUpDown numPasses_NumericUpDown;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.NumericUpDown normalScale_NumericUpDown;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.NumericUpDown maxStepHeight_NumericUpDown;
		private System.Windows.Forms.ComboBox mappingX_ComboBox;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.ComboBox mappingY_ComboBox;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.ComboBox mappingZ_ComboBox;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button preview_Button;
		private System.Windows.Forms.Button save_Button;
		private System.Windows.Forms.OpenFileDialog openImageDialog;
		private System.Windows.Forms.ToolTip infoToolTip;
		private System.Windows.Forms.StatusStrip statusStrip;
		private System.Windows.Forms.ToolStripStatusLabel statusLabel;
		private System.Windows.Forms.TabControl preview_TabControl;
		private System.Windows.Forms.TabPage normalMap_TabPage;
		private System.Windows.Forms.TabPage mask_TabPage;
		private System.Windows.Forms.PictureBox mask_PictureBox;
		private System.Windows.Forms.TabPage preview_TabPage;
		private System.Windows.Forms.PictureBox preview_PictureBox;
		private System.Windows.Forms.SaveFileDialog saveDisplacementMapDialog;
		private System.Windows.Forms.ToolStripProgressBar progressBar;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox zrange_ComboBox;
		private System.Windows.Forms.ComboBox edgeMode_ComboBox;
		private System.Windows.Forms.Label label11;
	}
}

