﻿using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media.Imaging;

namespace NormalToHeightGUI
{
	public enum ChannelMapping
	{
		Red,
		Green,
		Blue,
		Alpha,
		OneMinus_Red,
		OneMinus_Green,
		OneMinus_Blue,
		OneMinus_Alpha,
		None,
	}

	public enum ZRange
	{
		Full,
		Half,
		Clamped,
	}

	public enum EdgeMode
	{
		Free,
		Clamp,
		Wrap,
	}

	public partial class Form1 : Form
	{
		private string NormalMapPath_Temp;
		private string MaskPath_Temp;

		private string PreviewPath_Temp;
		private string PreviewNormalMapPath_Temp;
		private string PreviewMaskPath_Temp;

		private Task<NormalMapSize> LoadNormalMapTask;
		private Task<Size> LoadMaskTask;
		private Task GeneratePreviewTask;
		private Task GenerateDisplacementTask;

		public Form1()
		{
			InitializeComponent();

			mappingX_ComboBox.DataSource = Enum.GetValues(typeof(ChannelMapping)).Cast<ChannelMapping>().Where(e => e != ChannelMapping.None).ToArray();
			mappingY_ComboBox.DataSource = Enum.GetValues(typeof(ChannelMapping)).Cast<ChannelMapping>().Where(e => e != ChannelMapping.None).ToArray();
			mappingZ_ComboBox.DataSource = Enum.GetValues(typeof(ChannelMapping));
			zrange_ComboBox.DataSource = Enum.GetValues(typeof(ZRange));
			edgeMode_ComboBox.DataSource = Enum.GetValues(typeof(EdgeMode));

			NormalMapPath_Temp = Path.GetTempFileName();
			MaskPath_Temp = Path.GetTempFileName();
			PreviewNormalMapPath_Temp = Path.GetTempFileName();
			PreviewPath_Temp = Path.GetTempFileName();
			PreviewMaskPath_Temp = Path.GetTempFileName();

			preview_TabControl.TabPages.Remove(mask_TabPage);

			//Load up settings
			numPasses_NumericUpDown.Value = Properties.Settings.Default.NumPasses;
			scale_NumericUpDown.Value = Properties.Settings.Default.Scale;
			normalScale_NumericUpDown.Value = Properties.Settings.Default.NormalScale;
			normalise_CheckBox.Checked = Properties.Settings.Default.Normalise;
			maxStepHeight_NumericUpDown.Value = Properties.Settings.Default.MaxStepHeight;
			mappingX_ComboBox.SelectedIndex = Properties.Settings.Default.MappingX;
			mappingY_ComboBox.SelectedIndex = Properties.Settings.Default.MappingY;
			mappingZ_ComboBox.SelectedIndex = Properties.Settings.Default.MappingZ;
			zrange_ComboBox.SelectedIndex = Properties.Settings.Default.ZRange;
			edgeMode_ComboBox.SelectedIndex = Properties.Settings.Default.EdgeMode;
			maskFile_TextBox.Text = Properties.Settings.Default.Mask;
			inFile_TextBox.Text = Properties.Settings.Default.NormalMap;
			this.Size = Properties.Settings.Default.WindowSize;
		}

		private void DisableAllChildControls(Control ParentControl)
		{
			foreach(Control child in ParentControl.Controls)
			{
				child.Enabled = false;
				DisableAllChildControls(child);
			}
		}

		private void EnableAllChildControls(Control ParentControl)
		{
			foreach (Control child in ParentControl.Controls)
			{
				child.Enabled = true;
				EnableAllChildControls(child);
			}
		}

		private void EnableAllControls()
		{
			EnableAllChildControls(this);
			scale_NumericUpDown.Enabled = !normalise_CheckBox.Checked;
			zrange_ComboBox.Enabled = (ChannelMapping)mappingZ_ComboBox.SelectedValue != ChannelMapping.None;

			bool EnableButtons = LoadNormalMapTask != null && !LoadNormalMapTask.Result.IsEmpty;
			preview_Button.Enabled = EnableButtons;
			save_Button.Enabled = EnableButtons;
		}

		private void DisableAllControls()
		{
			DisableAllChildControls(this);
			statusStrip.Enabled = true;
		}

		private string GetMappingString(ChannelMapping mapping)
		{
			switch(mapping)
			{
				case ChannelMapping.Red: return "r";
				case ChannelMapping.Green: return "g";
				case ChannelMapping.Blue: return "b";
				case ChannelMapping.OneMinus_Red: return "fr";
				case ChannelMapping.OneMinus_Green: return "fg";
				case ChannelMapping.OneMinus_Blue: return "fb";
				case ChannelMapping.None: return "n";
			}

			return "r";
		}

		private string GetParams(string outputPath, bool preview)
		{
			StringBuilder Params = new StringBuilder();
			Params.AppendFormat(" \"{0}\"", preview ? PreviewNormalMapPath_Temp : NormalMapPath_Temp);
			Params.AppendFormat(" \"{0}\"", outputPath);
			if (normalise_CheckBox.Checked)
			{
				Params.Append(" -normalise");
			}
			else
			{
				Params.AppendFormat(" -scale {0:0.##}", scale_NumericUpDown.Value);
			}

			Params.AppendFormat(" -maxStepHeight {0:0.##}", maxStepHeight_NumericUpDown.Value);
			Params.AppendFormat(" -normalScale {0:0.###}", normalScale_NumericUpDown.Value);
			if (LoadMaskTask  != null && LoadMaskTask.Result != null)
			{
				Params.AppendFormat(" -mask \"{0}\"", preview ? PreviewMaskPath_Temp : MaskPath_Temp);
			}

			int NumPasses = decimal.ToInt32(numPasses_NumericUpDown.Value);
			if (preview)
			{
				NumPasses = Math.Min(NumPasses, 64);
				Params.Append(" -outputRaw");
			}
			Params.AppendFormat(" -numPasses {0}", NumPasses);

			ChannelMapping X = (ChannelMapping)mappingX_ComboBox.SelectedValue;
			ChannelMapping Y = (ChannelMapping)mappingY_ComboBox.SelectedValue;
			ChannelMapping Z = (ChannelMapping)mappingZ_ComboBox.SelectedValue;
			Params.AppendFormat(" -mapping X{0}Y{1}Z{2}", GetMappingString(X), GetMappingString(Y), GetMappingString(Z));

			switch ((ZRange)zrange_ComboBox.SelectedIndex)
			{
				case ZRange.Full: Params.Append(" -zrange full"); break;
				case ZRange.Half: Params.Append(" -zrange half"); break;
				case ZRange.Clamped: Params.Append(" -zrange clamped"); break;
			}

			switch ((EdgeMode)edgeMode_ComboBox.SelectedIndex)
			{
				case EdgeMode.Free: Params.Append(" -edgeMode free"); break;
				case EdgeMode.Clamp: Params.Append(" -edgeMode clamp"); break;
				case EdgeMode.Wrap: Params.Append(" -edgeMode wrap"); break;
			}

			return Params.ToString();
		}

		private async Task<NormalMapSize> LoadNormalMapAsync()
		{
			NormalMapSize NMapSize = new NormalMapSize();

			await Task.Run(() =>
			{
				using (FileStream FS = new FileStream(inFile_TextBox.Text, FileMode.Open, FileAccess.Read))
				{
					try
					{
						BitmapImage Img = null;
						Img = new BitmapImage();
						Img.BeginInit();
						Img.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
						Img.StreamSource = FS;
						Img.EndInit();

						if (Img != null)
						{
							NMapSize.FullSize.Width = Img.PixelWidth;
							NMapSize.FullSize.Height = Img.PixelHeight;

							//Save out the image as a Png
							using (FileStream OutFS = new FileStream(NormalMapPath_Temp, FileMode.Create, FileAccess.Write))
							{
								BitmapEncoder enc = new PngBitmapEncoder();
								enc.Frames.Add(BitmapFrame.Create(Img));
								enc.Save(OutFS);
							}

							//Make a smaller version for quicker previews
							BitmapSource MiniMap = null;

							int LongEdge = Math.Max(Img.PixelWidth, Img.PixelHeight);
							float Scale = 512.0f / LongEdge;
							if (Scale > 0.0f && Scale < 1.0f)
							{
								MiniMap = new TransformedBitmap(Img, new System.Windows.Media.ScaleTransform(Scale, Scale));
							}
							else
							{
								FormatConvertedBitmap FormattedMiniMap = new FormatConvertedBitmap();
								FormattedMiniMap.BeginInit();
								FormattedMiniMap.Source = Img;
								FormattedMiniMap.DestinationFormat = System.Windows.Media.PixelFormats.Bgra32;
								FormattedMiniMap.EndInit();

								MiniMap = FormattedMiniMap;
							}

							NMapSize.PreviewSize.Width = MiniMap.PixelWidth;
							NMapSize.PreviewSize.Height = MiniMap.PixelHeight;
							using (FileStream OutFS = new FileStream(PreviewNormalMapPath_Temp, FileMode.Create, FileAccess.ReadWrite))
							{
								BitmapEncoder enc = new PngBitmapEncoder();
								enc.Frames.Add(BitmapFrame.Create(MiniMap));
								enc.Save(OutFS);

								//Preview image
								normalMap_PictureBox.Image = Image.FromStream(OutFS);
							}
						}
					}
					catch (NotSupportedException)
					{
						normalMap_PictureBox.Image = null;
					}
				}
			});

			preview_Button.Enabled = !NMapSize.IsEmpty;
			save_Button.Enabled = !NMapSize.IsEmpty;

			return NMapSize;
		}

		private async Task<Size> LoadMaskAsync()
		{
			Size ImgSize = new Size();

			await Task.Run(() =>
			{
				using (FileStream FS = new FileStream(maskFile_TextBox.Text, FileMode.Open, FileAccess.Read))
				{
					try
					{
						BitmapImage Img = null;
						Img = new BitmapImage();
						Img.BeginInit();
						Img.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
						Img.StreamSource = FS;
						Img.EndInit();

						if (Img != null)
						{
							ImgSize.Width = Img.PixelWidth;
							ImgSize.Height = Img.PixelHeight;

							//Save out the image as a Png
							using (FileStream OutFS = new FileStream(MaskPath_Temp, FileMode.Create, FileAccess.Write))
							{
								BitmapEncoder enc = new PngBitmapEncoder();
								enc.Frames.Add(BitmapFrame.Create(Img));
								enc.Save(OutFS);
							}

							//Make a smaller version for quicker previews
							BitmapSource MiniMap = null;

							int LongEdge = Math.Max(Img.PixelWidth, Img.PixelHeight);
							float Scale = 512.0f / LongEdge;
							if (Scale > 0.0f && Scale < 1.0f)
							{
								MiniMap = new TransformedBitmap(Img, new System.Windows.Media.ScaleTransform(Scale, Scale));
							}
							else
							{
								FormatConvertedBitmap FormattedMiniMap = new FormatConvertedBitmap();
								FormattedMiniMap.BeginInit();
								FormattedMiniMap.Source = Img;
								FormattedMiniMap.DestinationFormat = System.Windows.Media.PixelFormats.Bgr24;
								FormattedMiniMap.EndInit();

								MiniMap = FormattedMiniMap;
							}

							using (FileStream OutFS = new FileStream(PreviewMaskPath_Temp, FileMode.Create, FileAccess.ReadWrite))
							{
								PngBitmapEncoder enc = new PngBitmapEncoder();
								enc.Frames.Add(BitmapFrame.Create(MiniMap));
								enc.Save(OutFS);

								//Preview image
								mask_PictureBox.Image = Image.FromStream(OutFS);
							}
						}
					}
					catch (NotSupportedException)
					{
						//Invalid file
						mask_PictureBox.Image = null;
					}
				}
			});

			if (!ImgSize.IsEmpty)
			{
				//Add the tab page back in
				if (!preview_TabControl.TabPages.Contains(mask_TabPage))
				{
					preview_TabControl.TabPages.Insert(1, mask_TabPage);
				}
			}
			else
			{
				//Make sure it is removed
				preview_TabControl.TabPages.Remove(mask_TabPage);
			}

			return ImgSize;
		}

		private Task<int> WaitForProcessAsync(ProcessStartInfo Proc, IProgress<float> Progress)
		{
			Process process = new Process();
			process.StartInfo = Proc;
			process.StartInfo.RedirectStandardOutput = true;
			process.StartInfo.UseShellExecute = false;
			process.EnableRaisingEvents = true;
			TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();
			process.Exited += (s, ea) => tcs.SetResult(process.ExitCode);
			process.OutputDataReceived += (s, ea) =>
			{
				if (ea.Data != null)
				{
					float UpdatePercent = GetUpdatePercantageFromOuput(ea.Data);
					if (UpdatePercent >= 0)
					{
						Progress.Report(UpdatePercent);
					}
				}
			};

			if (!process.Start())
			{
				tcs.SetResult(-1);
			}
			process.BeginOutputReadLine();

			Deactivate += OneTimeActivate;

			return tcs.Task;
		}

		private async Task WaitForSourceImagesAsync()
		{
			if (LoadNormalMapTask != null)
				await LoadNormalMapTask;

			if (LoadMaskTask != null)
				await LoadMaskTask;
		}

		private async Task GeneratePreview(IProgress<float> Progress)
		{
			DisableAllControls();
			statusLabel.Text = "Generating preview...";
			statusLabel.Visible = true;
			progressBar.Visible = true;
			progressBar.Value = 0;

			await WaitForSourceImagesAsync();

			if (LoadNormalMapTask != null && !LoadNormalMapTask.Result.IsEmpty)
			{
				NormalMapSize NMapSize = LoadNormalMapTask.Result;

				string Params = GetParams(PreviewPath_Temp, true);

				ProcessStartInfo Proc = new ProcessStartInfo();
				Proc.FileName = Path.Combine(Directory.GetCurrentDirectory(), "NormalToHeight.exe");
				Proc.Arguments = Params;
				Proc.WindowStyle = ProcessWindowStyle.Hidden;
				Proc.CreateNoWindow = true;
				await WaitForProcessAsync(Proc, Progress);

				await Task.Run(() =>
				{
					MemoryStream MS = new MemoryStream(NMapSize.PreviewSize.Width * NMapSize.PreviewSize.Height * 2);
					FileStream FS = new FileStream(PreviewPath_Temp, FileMode.Open);
					FS.CopyTo(MS);
					FS.Close();

					byte[] RawImageData = MS.ToArray();
					preview_PictureBox.Image = Gray16To8bppIndexed(RawImageData, NMapSize.PreviewSize.Width, NMapSize.PreviewSize.Height);
				});

				preview_TabControl.SelectedTab = preview_TabPage;
			}

			progressBar.Visible = false;
			statusLabel.Visible = false;
			EnableAllControls();
		}

		private async Task GenerateDisplacementMap(string outputPath, IProgress<float> Progress)
		{
			DisableAllControls();
			statusLabel.Text = "Generating displacement map...";
			statusLabel.Visible = true;
			progressBar.Visible = true;
			progressBar.Value = 0;

			await WaitForSourceImagesAsync();

			if (LoadNormalMapTask != null && !LoadNormalMapTask.Result.IsEmpty)
			{
				NormalMapSize NMapSize = LoadNormalMapTask.Result;

				string Params = GetParams(outputPath, false);

				ProcessStartInfo Proc = new ProcessStartInfo();
				Proc.FileName = Path.Combine(Directory.GetCurrentDirectory(), "NormalToHeight.exe");
				Proc.Arguments = Params;
				Proc.WindowStyle = ProcessWindowStyle.Hidden;
				Proc.CreateNoWindow = true;
				await WaitForProcessAsync(Proc, Progress);
			}

			progressBar.Visible = false;
			statusLabel.Text = String.Format("Saved '{0}'", outputPath); ;
			using (FileStream FS = new FileStream(outputPath, FileMode.Open, FileAccess.Read))
			{
				preview_PictureBox.Image = Image.FromStream(FS);
			}

			preview_TabControl.SelectedTab = preview_TabPage;
			EnableAllControls();
		}

		private Bitmap Gray16To8bppIndexed(byte[] data, int width, int height)
		{
			byte[] IndexedImageData = new byte[width * height];

			if (data.Length == IndexedImageData.Length * 2)
			{
				for (int i = 0; i < IndexedImageData.Length; ++i)
					IndexedImageData[i] = data[i * 2];
			}
			else
			{
				//Unexpected data length
				for (int i = 0; i < IndexedImageData.Length; ++i)
					IndexedImageData[i] = 255;
			}

			Bitmap BmpOut = new Bitmap(width, height, PixelFormat.Format8bppIndexed);
			BitmapData BmpData = BmpOut.LockBits(new Rectangle(0,0,width,height), ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);
			Marshal.Copy(IndexedImageData, 0, BmpData.Scan0, IndexedImageData.Length);
			BmpOut.UnlockBits(BmpData);

			ColorPalette GrayPalette = BmpOut.Palette;
			Color[] GrayColors = GrayPalette.Entries;
			for (int i = 0; i < GrayColors.Length; i++)
				GrayColors[i] = Color.FromArgb(i, i, i);
			BmpOut.Palette = GrayPalette;

			return BmpOut;
		}

		private void inFileBrowse_Button_Click(object sender, EventArgs e)
		{
			DialogResult Result = openImageDialog.ShowDialog();
			if (Result == DialogResult.OK)
			{
				inFile_TextBox.Text = openImageDialog.FileName;
				inFile_TextBox.Select(0, 0);
			}
		}

		private void maskFileBrowse_Button_Click(object sender, EventArgs e)
		{
			DialogResult Result = openImageDialog.ShowDialog();
			if (Result == DialogResult.OK)
			{
				maskFile_TextBox.Text = openImageDialog.FileName;
				maskFile_TextBox.Select(0, 0);
			}
		}

		private void normalise_CheckBox_CheckedChanged(object sender, EventArgs e)
		{
			scale_NumericUpDown.Enabled = !normalise_CheckBox.Checked;
		}

		private async void inFile_TextBox_TextChanged(object sender, EventArgs e)
		{
			if (LoadNormalMapTask != null)
			{
				await LoadNormalMapTask;
			}

			if (File.Exists(inFile_TextBox.Text))
			{
				LoadNormalMapTask = LoadNormalMapAsync();
			}
			else
			{
				LoadNormalMapTask = null;
				normalMap_PictureBox.Image = null;
				preview_Button.Enabled = false;
				save_Button.Enabled = false;
			}
		}

		private async void maskFile_TextBox_TextChanged(object sender, EventArgs e)
		{
			if (LoadMaskTask != null)
			{
				await LoadMaskTask;
			}

			if (File.Exists(maskFile_TextBox.Text))
			{
				LoadMaskTask = LoadMaskAsync();
			}
			else
			{
				LoadMaskTask = null;
				preview_TabControl.TabPages.Remove(mask_TabPage);
				mask_PictureBox.Image = null;
			}
		}

		private async void preview_Button_Click(object sender, EventArgs e)
		{
			if (GeneratePreviewTask != null)
			{
				await GeneratePreviewTask;
			}
			if (GenerateDisplacementTask != null)
			{
				await GenerateDisplacementTask;
			}

			var Progress = new Progress<float>(percent =>
			{
				if (progressBar != null)
				{
					progressBar.Value = (int)Math.Round(percent * progressBar.Maximum);
				}
			});

			GeneratePreviewTask = GeneratePreview(Progress);
		}

		private async void save_Button_Click(object sender, EventArgs e)
		{
			DialogResult Result = saveDisplacementMapDialog.ShowDialog();
			if (Result == DialogResult.OK)
			{
				if (GeneratePreviewTask != null)
				{
					await GeneratePreviewTask;
				}
				if (GenerateDisplacementTask != null)
				{
					await GenerateDisplacementTask;
				}

				var Progress = new Progress<float>(percent =>
				{
					if (progressBar != null)
					{
						progressBar.Value = (int)Math.Round(percent * progressBar.Maximum);
					}
				});

				GenerateDisplacementTask = GenerateDisplacementMap(saveDisplacementMapDialog.FileName, Progress);
			}
		}

		private float GetUpdatePercantageFromOuput(string ouput)
		{
			if (ouput.StartsWith("Built shaders", StringComparison.InvariantCultureIgnoreCase))
			{
				return 0.05f;
			}
			else if (ouput.StartsWith("Loaded images", StringComparison.InvariantCultureIgnoreCase))
			{
				return 0.1f;
			}
			else if (ouput.StartsWith("Saving file", StringComparison.InvariantCultureIgnoreCase))
			{
				return 0.95f;
			}
			else if (ouput.StartsWith("Rendering", StringComparison.InvariantCultureIgnoreCase))
			{
				int Start = ouput.LastIndexOf(' ') + 1;
				int End = ouput.LastIndexOf('%');
				string PercentageString = ouput.Substring(Start, End - Start);
				float Percentage = 0.0f;
				if (float.TryParse(PercentageString, out Percentage))
				{
					return 0.1f + (0.85f * (Percentage / 100.0f));
				}
			}

			return -1;
		}

		private void OneTimeActivate(object sender, EventArgs e)
		{
			Activate();
			Deactivate -= OneTimeActivate;
		}

		private void Form1_FormClosed(object sender, FormClosedEventArgs e)
		{
			//Delete our temporary files
			if (File.Exists(NormalMapPath_Temp))
				File.Delete(NormalMapPath_Temp);
			if (File.Exists(MaskPath_Temp))
				File.Delete(MaskPath_Temp);
			if (File.Exists(PreviewPath_Temp))
				File.Delete(PreviewPath_Temp);
			if (File.Exists(PreviewNormalMapPath_Temp))
				File.Delete(PreviewNormalMapPath_Temp);
			if (File.Exists(PreviewMaskPath_Temp))
				File.Delete(PreviewMaskPath_Temp);

			//Save settings
			if (ModifierKeys.HasFlag(Keys.Control))
			{
				Properties.Settings.Default.Reset();
			}
			else
			{
				Properties.Settings.Default.NumPasses = decimal.ToInt32(numPasses_NumericUpDown.Value);
				Properties.Settings.Default.Scale = scale_NumericUpDown.Value;
				Properties.Settings.Default.NormalScale = normalScale_NumericUpDown.Value;
				Properties.Settings.Default.Normalise = normalise_CheckBox.Checked;
				Properties.Settings.Default.MaxStepHeight = decimal.ToInt32(maxStepHeight_NumericUpDown.Value);
				Properties.Settings.Default.MappingX = mappingX_ComboBox.SelectedIndex;
				Properties.Settings.Default.MappingY = mappingY_ComboBox.SelectedIndex;
				Properties.Settings.Default.MappingZ = mappingZ_ComboBox.SelectedIndex;
				Properties.Settings.Default.ZRange = zrange_ComboBox.SelectedIndex;
				Properties.Settings.Default.EdgeMode = edgeMode_ComboBox.SelectedIndex;
				Properties.Settings.Default.Mask = maskFile_TextBox.Text;
				Properties.Settings.Default.NormalMap = inFile_TextBox.Text;
				Properties.Settings.Default.WindowSize = this.Size;
			}
			Properties.Settings.Default.Save();
		}

		private void mappingZ_ComboBox_SelectedValueChanged(object sender, EventArgs e)
		{
			zrange_ComboBox.Enabled = (ChannelMapping)mappingZ_ComboBox.SelectedValue != ChannelMapping.None;
		}
	}

	public class NormalMapSize
	{
		public Size FullSize = new Size();
		public Size PreviewSize = new Size();

		public bool IsEmpty { get { return FullSize.IsEmpty; } }
	}
}
